//
// Created by Ralo on 14/04/2019.
//
#include "definiciones.h"
#include "auxiliares.h"

/*Auxiliares del proc 1*/

bool esValida(eph_h &th, eph_i &ti) {
    return !vacia(th) && !vacia(ti) && esMatriz(ti) && esMatriz(th) && cantidadCorrectaDeColumnasI(ti) &&
           cantidadCorrectaDeColumnasH(th) && !hayIndividuosSinHogares(ti, th) && !hayHogaresSinIndividuos(ti, th) &&
           !hayRepetidosI(ti) && !hayRepetidosH(th) && mismoAnioYTrimestre(ti, th) &&
           menosDe21MiembrosPorHogar(th, ti) && cantidadValidaDormitorios(th) && valoresEnRangoI(ti) &&
           valoresEnRangoH(th);
}

bool esMatriz(vector<vector<dato> > t) {
    int length = t[0].size();
    int i = 1;
    while (i < t.size() && t[i].size() == length) {
        i++;
    }
    return i == t.size();
}

bool vacia(vector<vector<dato> > t) {
    return t.size() == 0;
}

bool cantidadCorrectaDeColumnasI(eph_i ti) {
    int i = 0;
    while (i < ti.size() && ti[i].size() == FILAS_INDIVIDUO) {
        i++;
    }
    return i == ti.size();
}

bool cantidadCorrectaDeColumnasH(eph_h th) {
    int i = 0;
    while (i < th.size() && th[i].size() == FILAS_HOGAR) {
        i++;
    }
    return i == th.size();
}

bool hayHogarConCodigo(eph_h th, int c) {
    int i = 0;
    while (i < th.size() && th[i][HOGCODUSU] != c) {
        i++;
    }
    return i < th.size();
}

bool hayIndividuosSinHogares(eph_i ti, eph_h th) {
    int i = 0;
    while (i < ti.size() && hayHogarConCodigo(th, ti[i][INDCODUSU])) {
        i++;
    }
    return i < ti.size();
}

bool hayIndividuoConCodigo(eph_i ti, int c) {
    int i = 0;
    while (i < ti.size() && ti[i][INDCODUSU] != c) {
        i++;
    }
    return i < ti.size();
}

bool hayHogaresSinIndividuos(eph_i ti, eph_h th) {
    int i = 0;
    while (i < th.size() && hayIndividuoConCodigo(ti, th[i][HOGCODUSU])) {
        i++;
    }
    return i < th.size();
}

bool hayRepetidosI(eph_i ti) {
    int i = 0;
    int j = 1;
    while (i < ti.size() && j == i + 1) {
        while (j < ti.size() && !mismoCodusuYComponente(ti[i], ti[j])) {
            j++;
        }
        i++;
        if (j == ti.size()) { //si j llegó al final de la tabla sin encontrar repetidos, se reinicia el contador
            j = i + 1;
        }
    }
    return i < ti.size();
}

bool mismoCodusuYComponente(individuo i1, individuo i2) {
    return i1[INDCODUSU] == i2[INDCODUSU] && i1[COMPONENTE] == i2[COMPONENTE];
}

bool hayRepetidosH(eph_h th) {
    int i = 0;
    int j = 1;
    while (i < th.size() && j == i + 1) {
        while (j < th.size() && th[i][HOGCODUSU] != th[j][HOGCODUSU]) {
            j++;
        }
        i++;
        if (j == th.size()) { //si j llegó al final de la tabla sin encontrar repetidos, se reinicia el contador
            j = i + 1;
        }
    }
    return i < th.size();
}

bool mismoAnioYTrimestre(eph_i ti, eph_h th) {
    int anio = ti[0][INDANIO];
    int trimestre = ti[0][INDTRIMESTRE];
    int i = 0;
    while (i < ti.size() && ti[i][INDANIO] == anio && ti[i][INDTRIMESTRE] == trimestre) {
        i++;
    }
    int j = 0;
    while (j < th.size() && th[j][HOGANIO] == anio && th[j][HOGTRIMESTRE] == trimestre) {
        j++;
    }
    return i == ti.size() && j == th.size();
}

bool menosDe21MiembrosPorHogar(eph_h th, eph_i ti) {
    int i = 0;
    while (i < th.size() && cantHabitantes(th[i], ti) < 21) {
        i++;
    }
    return i == th.size();
}

int cantHabitantes(hogar h, eph_i ti) {
    int res = 0;
    for (int i = 0; i < ti.size(); i++) {
        if (esSuHogar(h, ti[i])) {
            res++;
        }
    }
    return res;
}

bool esSuHogar(hogar h, individuo i) {
    return h[HOGCODUSU] == i[INDCODUSU];
}

bool cantidadValidaDormitorios(eph_h th) {
    int i = 0;
    while (i < th.size() && th[i][IV2] >= th[i][II2]) {
        i++;
    }
    return i == th.size();
}

bool valoresEnRangoI(eph_i ti) {
    int i = 0;
    while (i < ti.size() && individuoValido(ti[i])) {
        i++;
    }
    return i == ti.size();
}

bool individuoValido(individuo i) {
    bool infgeneral = i[INDCODUSU] > 0 && i[COMPONENTE] > 0 && (i[INDTRIMESTRE] > 0 && i[INDTRIMESTRE] <= 4);
    bool infpersonal = (i[CH4] == 1 || i[CH4] == 2) && (i[CH6] >= 0) && (i[NIVEL_ED] == 0 || i[NIVEL_ED] == 1);
    bool inflaboral = (-1 <= i[ESTADO] && i[ESTADO] <= 1) && (i[CAT_OCUP] >= 0 && i[CAT_OCUP] <= 4) &&
                      (i[p47T] >= 0 || i[p47T] == -1) &&
                      (i[PP04G] > 0 && i[PP04G] <= 10);
    bool res = infgeneral && infpersonal && inflaboral;
    return res;
}

bool valoresEnRangoH(eph_h th) {
    int i = 0;
    while (i < th.size() && hogarValido(th[i])) {
        i++;
    }
    return i == th.size();
}

bool hogarValido(hogar h) {
    bool infgeneral =
            h[HOGCODUSU] > 0 && (h[HOGTRIMESTRE] > 0 && h[HOGTRIMESTRE] <= 4) && (h[II7] > 0 && h[II7] <= 3) &&
            (h[REGION] > 0 && h[REGION] <= 6);
    bool infdemograf =
            (h[MAS_500] == 0 || h[MAS_500] == 1) && (h[IV1] > 0 && h[IV1] <= 5) && h[IV2] > 0 && h[II2] > 0 &&
            (h[II3] == 1 || h[II3] == 2);
    bool res = infgeneral && infdemograf;
    return res;
}


/*Auxiliares del proc 2*/

bool esCasa(hogar h) {
    return h[IV1] == 1;
}

int cantHogaresCasaConNHabitaciones(eph_h &th, int region, int hab) {
    int sum = 0;
    int i = 0;
    while (i < th.size()) {
        if (esCasa(th[i]) && th[i][IV2] == hab && th[i][REGION] == region) {
            sum++;
        }
        i++;
    }
    return sum;
}
/*Cuenta la cantidad de hogares con un número dado de habitaciones*/

int cantMaxHabitacionesEnHogEnReg(eph_h &th, int reg) {
    int i = 0;
    int res = 1;
    while (i < th.size()) {
        if (esCasa(th[i]) && res < th[i][IV2] && th[i][REGION] == reg) {
            res = th[i][IV2];
        }
        i++;
    }
    return res;
}
/*Dada la tabla y una región, devuelve la cantidad máxima de habitaciones en hogares que se encuentran en dicah región*/

/*Auxiliares del proc 3*/

bool esHogarValido(hogar h, int region) {
    return esCasa(h) && h[REGION] == region && h[MAS_500] == 0;
}

int cantHogaresValidos(eph_h th, int region) {
    int res = 0;
    for (int i = 0; i < th.size(); i++) {
        if (esHogarValido(th[i], region)) {
            res++;
        }
    }
    return res;
}

bool hogarConHacinamientoCritico(hogar h, eph_i &ti) {
    return cantHabitantes(h, ti) > 3 * h[II2];
}

int cantHogaresValidosConHC(eph_h th, eph_i &ti, int region) {
    int res = 0;
    for (int i = 0; i < th.size(); i++) {
        if (esHogarValido(th[i], region) && hogarConHacinamientoCritico(th[i], ti)) {
            res++;
        }
    }
    return res;
}

float proporcionDeCasasConHC(eph_h &th, eph_i &ti, int region) {
    float res = 0;
    if (cantHogaresValidos(th, region) > 0) {
        res = ((float) cantHogaresValidosConHC(th, ti, region) / cantHogaresValidos(th, region));
    }
    return res;
}


/*Auxiliares del proc 4*/

bool trabaja(individuo i) {
    return i[ESTADO] == OCUPADO;
}

bool esDeCiudadGrande(individuo i, eph_h th) {
    int j = 0;
    while (j < th.size() && (!esSuHogar(th[j], i) || th[j][MAS_500] == 0)) {
        j++;
    }
    return j < th.size();
}

bool esCasaODepartamento(hogar h) {
    return h[IV1] == CASA || h[IV1] == 2; //No está incluida la definición DEPARTAMENTO = 2 en definiciones.h
}

bool realizaSusTareasEnEsteHogar(individuo i) {
    return i[PP04G] == EN_ESTE_HOGAR;
}

bool tieneEspaciosReservadosParaElTrabajo(hogar h) {
    return h[II3] == 1;
}

bool suHogarEsCasaODepartamento(individuo i, eph_h th) {
    int j = 0;
    while (j < th.size() && (!esSuHogar(th[j], i) || !esCasaODepartamento(th[j]))) {
        j++;
    }
    return j < th.size();
}

bool suHogarTieneEspaciosReservadosParaElTrabajo(individuo i, eph_h &th) {
    int j = 0;
    while (j < th.size() && (!esSuHogar(th[j], i) || !tieneEspaciosReservadosParaElTrabajo(th[j]))) {
        j++;
    }
    return j < th.size();
}

bool individuoEnHogarValido(individuo i, eph_h th) {
    return esDeCiudadGrande(i, th) && suHogarEsCasaODepartamento(i, th);
}

bool trabajaEnSuVivienda(individuo i, eph_h th) {
    return realizaSusTareasEnEsteHogar(i) && suHogarTieneEspaciosReservadosParaElTrabajo(i, th);
}

int cantIndividuosQueTrabajan(eph_h th, eph_i ti) {
    int res = 0;
    for (int i = 0; i < ti.size(); i++) {
        if (trabaja(ti[i]) && individuoEnHogarValido(ti[i], th)) {
            res++;
        }
    }
    return res;
}

int cantIndividuosTrabajandoEnSuvivienda(eph_h th, eph_i ti) {
    int res = 0;
    for (int i = 0; i < ti.size(); i++) {
        if (trabaja(ti[i]) && trabajaEnSuVivienda(ti[i], th) && individuoEnHogarValido(ti[i], th)) {
            res++;
        }
    }
    return res;
}

float proporcionTeleworking(eph_h &th, eph_i &ti) {
    float res = 0;
    if (cantIndividuosQueTrabajan(th, ti) > 0) {
        res = ((float) cantIndividuosTrabajandoEnSuvivienda(th, ti) / cantIndividuosQueTrabajan(th, ti));
    }
    return res;
}


/*Auxiliares del proc 5*/

vector<vector<int>> auxOrdenarPorRegion(eph_h th) {
    int j = th.size() - 1;
    while (j > 0) {
        int i = 0;
        while (i < j) {
            if (th[i][REGION] > th[i + 1][REGION]) {
                vector<int> aux = th[i + 1];
                th[i + 1] = th[i];
                th[i] = aux;
            }
            i++;
        }
        j--;
    }
    return th;
}
/*Ordena la tabla de hogares por región*/

vector<vector<int>> auxOrdenarPorCodusuEnRegion(eph_h th) {
    th = auxOrdenarPorRegion(th);
    int k = 1;
    while (k < 7) {
        int j = th.size() - 1;
        while (j > 0) {
            int i = 0;
            while (i < j) {
                if (th[i][HOGCODUSU] > th[i + 1][HOGCODUSU] && th[i][REGION] == k && th[i + 1][REGION] == k) {
                    vector<int> aux = th[i + 1];
                    th[i + 1] = th[i];
                    th[i] = aux;
                }
                i++;
            }
            j--;
        }
        k++;
    }
    return th;
}
/*Ordena por hogcodusu dentro de cada bloque de región. Trabaja con la tabla ya ordenada por región*/

vector<vector<int>> auxOrdenarPorCodusuIndividuos(eph_h th, eph_i ti) {
    th = auxOrdenarPorCodusuEnRegion(auxOrdenarPorRegion(th));
    int i = 0;
    vector<vector<int>> aux;
    while (i < th.size()) {
        int j = 0;
        while (j < ti.size()) {
            if (th[i][HOGCODUSU] == ti[j][INDCODUSU]) {
                aux.push_back(ti[j]);
            }
            j++;
        }
        i++;
    }
    ti = aux;
    return ti;
}
/*Ordena la tabla de individuos de acuerdo a la posición de los hogcodusu en la tabla de hogares*/

int finDeBloque(int n, eph_i ti) {
    int i = n;
    while (i < ti.size() && ti[n][INDCODUSU] == ti[i][INDCODUSU]) {
        i++;
    }
    return i - 1;
}
  /*Esta función se fija el incodusu en la posición n. Luego, se fija hasta qué posición comparten dicho indcodusu,*/
  /*y devuelve el último, marcando de dónde a dónde los individuos comparten el hogar*/

vector<vector<int>> auxOrdenarPorComponenteAIndividuos(eph_h &th, eph_i ti) {
    ti = auxOrdenarPorCodusuIndividuos(th, ti);
    int i = 0;
    while (i < ti.size()) {
        int j = i;
        while (j <= finDeBloque(i, ti)) {
            int k = j;
            while (k >= i) {
                int l = i;
                while (l < k) {
                    if (ti[l][COMPONENTE] > ti[l + 1][COMPONENTE]) {
                        vector<int> aux = ti[l + 1];
                        ti[l + 1] = ti[l];
                        ti[l] = aux;
                    }
                    l++;
                }
                k--;
            }
            j++;
        }
        i = finDeBloque(i, ti) + 1;
    }
    return ti;
}
/*Una vez ordenada según codusu, ordena la tabla de individuos por componente dentro de cada bloque*/

/*Auxiliares del proc 6*/

int ingresos(hogar &h, eph_i &ti) {
    int res = 0;
    for (int i = 0; i < ti.size(); i++) {
        if (ti[i][INDCODUSU] == h[HOGCODUSU] && ti[i][p47T] > -1) {
            res = res + ti[i][p47T];
        }
    }
    return res;
}

int difIngresos(eph_i &ti, hogar h1, hogar h2) {
    return ingresos(h2, ti) - ingresos(h1, ti);
}

/*Auxiliar para selection sort*/
int indiceDeHogarConIngresoMinEnSubsec(eph_h &th, eph_i ti, int l, int r) {
    int i = r;
    int res = i;
    while (i > l) {
        if (ingresos(th[i - 1], ti) < ingresos(th[res], ti)) {
            res = i - 1;
        }
        i--;
    }
    return res;
}

void ordenarTablaHogaresPorIngresos(eph_h &th, eph_i &ti) {
    int i = 0;
    while (i < th.size()) {
        hogar aux = th[i];
        th[i] = th[indiceDeHogarConIngresoMinEnSubsec(th, ti, i, th.size() - 1)];
        th[indiceDeHogarConIngresoMinEnSubsec(th, ti, i, th.size() - 1)] = aux;
        i++;
    }
}

/*Toma la diferencia entre los hogares en las posiciones i y j y luego recorre la tabla hogares para generar una
muestra homogénea con esa diferencia*/
vector<hogar> generarMuestraHomogeneaConDifEntreHogIyHogJ(eph_h th, eph_i ti, int i, int j) {
    vector<hogar> res;
    int dif = difIngresos(ti, th[i], th[j]);
    res.push_back(th[i]);
    res.push_back(th[j]);
    for (int k = j + 1; k < th.size(); k++) {
        if (difIngresos(ti, res[res.size() - 1], th[k]) == dif && dif > 0) {
            res.push_back(th[k]);
        }
    }
    return res;
}

/*A partir de una tabla hogares ordenada por ingresos, recorre todos los pares de hogares (i y j) en la tabla hogares.
Si con la diferencia de ingresos entre un hogar i y hogar j se puede generar una muestra homogénea de longitud mayor o
igual a 3, la incluye en un vector que contiene todas las muestras homogéneas posibles que cumplen con lo pedido. */
vector<vector<hogar> > generarMuestrasHomogeneas(eph_h &th, eph_i &ti) {
    vector<vector<hogar> > res;
    int i = 0;
    int j = i + 1;
    while (i < th.size() - 1) {
        while (j < th.size()) {
            vector<hogar> muestraHomogeneaConDifEntreHogIyHogJ = generarMuestraHomogeneaConDifEntreHogIyHogJ(th, ti, i, j);
            if (muestraHomogeneaConDifEntreHogIyHogJ.size() >= 3) {
                res.push_back(muestraHomogeneaConDifEntreHogIyHogJ);
            }
            j++;
        }
        i++;
        j = i + 1;
    }
    return res;
}

bool existeSolucionMuestraHomogeneaConAlMenos3(eph_h th, eph_i ti) {
    return generarMuestrasHomogeneas(th, ti).size() > 0;
}

vector<hogar> mayorMuestraHomogenea(eph_h th, eph_i ti) {
    vector<vector<hogar> > muestrasHomogeneas = generarMuestrasHomogeneas(th, ti);
    vector<hogar> res = muestrasHomogeneas[0];
    int i = 1;
    while (i < muestrasHomogeneas.size()) {
        if (res.size() < muestrasHomogeneas[i].size()) {
            res = muestrasHomogeneas[i];
        }
        i++;
    }
    return res;
}


/*Auxiliares del proc 7*/

void modificaRegionGBAaPampeana(eph_h &th, eph_i &ti) {
    for (int i = 0; i < th.size(); i++) {
        if (th[i][REGION] == GBA) {
            th[i][REGION] = PAMPEANA;
        }
    }
}


/*Auxiliares del proc 8*/

float areaIgualdadTotal = 0.5;

float ingresosPerCapitaEnHogar(hogar h, eph_i ti) {
    return ((float) ingresos(h, ti) / cantHabitantes(h, ti));
}

float ingresosHasta(int n, eph_h th, eph_i ti) {
    float res = 0;
    for (int i = 0; i <= n; i++) {
        res = res + ingresosPerCapitaEnHogar(th[i], ti);
    }
    return res;
}

float integralDeIngresosObservados(eph_h &th, eph_i &ti) {
    float res = 0;
    for (int i = 0; i < th.size(); i++) {
        res = res + ingresosHasta(i - 1, th, ti) / ingresosHasta(th.size() - 1, th, ti) +
              0.5 * (ingresosPerCapitaEnHogar(th[i], ti) / ingresosHasta(th.size() - 1, th, ti));
    }
    return res / th.size();
}

float areaObservada(eph_h &th, eph_i &ti) {
    return areaIgualdadTotal - integralDeIngresosObservados(th, ti);
}

/*Auxiliar para selection sort*/
int indiceDeHogConIngresoMinPerCapitaEnSubsec(eph_h &th, eph_i &ti, int l, int r) {
    int i = r;
    int res = i;
    while (i > l) {
        if (ingresosPerCapitaEnHogar(th[i - 1], ti) < ingresosPerCapitaEnHogar(th[res], ti)) {
            res = i - 1;
        }
        i--;
    }
    return res;
}

void ordenarTablaHogaresPorIngresosPerCapita(eph_h th, eph_i ti) {
    int i = 0;
    while (i < th.size()) {
        hogar aux = th[i];
        th[i] = th[indiceDeHogConIngresoMinPerCapitaEnSubsec(th, ti, i, th.size() - 1)];
        th[indiceDeHogConIngresoMinPerCapitaEnSubsec(th, ti, i, th.size() - 1)] = aux;
        i++;
    }
}

float calcularIndiceDeGini(eph_h th, eph_i ti){
    return areaObservada(th,ti)/areaIgualdadTotal;
}