//
// Created by leticia on 07/05/19.
//

#ifndef SOLUCION_AUXILIARES_H
#define SOLUCION_AUXILIARES_H

#include "definiciones.h"

/*Proc 1*/
bool esValida(eph_h &th, eph_i &ti); //OK
bool esMatriz (vector<vector<dato> > t); //OK
bool vacia (vector<vector<dato> > t); //OK
bool cantidadCorrectaDeColumnasI (eph_i ti); //OK
bool cantidadCorrectaDeColumnasH (eph_h th); //OK
bool hayHogarConCodigo(eph_h th, int c); //OK
bool hayIndividuosSinHogares(eph_i ti, eph_h th); //OK
bool hayIndividuoConCodigo(eph_i ti, int c); //OK
bool hayHogaresSinIndividuos(eph_i ti, eph_h th); //OK
bool hayRepetidosI(eph_i ti); //OK
bool mismoCodusuYComponente(individuo i1, individuo i2); //OK
bool hayRepetidosH(eph_h th); //OK
bool mismoAnioYTrimestre(eph_i ti, eph_h th); //OK
bool menosDe21MiembrosPorHogar(eph_h th, eph_i ti); //OK
int cantHabitantes(hogar h, eph_i ti); //OK
bool esSuHogar(hogar h, individuo i); //OK
bool cantidadValidaDormitorios(eph_h th); //OK
bool valoresEnRangoI(eph_i ti); //OK
bool individuoValido(individuo i); //OK
bool valoresEnRangoH(eph_h th); //OK
bool hogarValido(hogar h); //OK

/*Proc 2*/
bool esCasa (hogar h);
int cantHogaresCasaConNHabitaciones(eph_h &th, int reg, int hab);
int cantMaxHabitacionesEnHogEnReg(eph_h &th, int reg);

/*Proc 3*/
bool esHogarValido(hogar h, int region);
int cantHogaresValidos(eph_h th, int region);
bool hogarConHacinamientoCritico (hogar h, eph_i &ti);
int cantHogaresValidosConHC(eph_h th, eph_i &ti, int region);
float proporcionDeCasasConHC(eph_h &th, eph_i &ti, int region);

/*Proc 4*/
bool trabaja(individuo i);
bool esDeCiudadGrande(individuo i, eph_h th);
bool esCasaODepartamento(hogar h);
bool realizaSusTareasEnEsteHogar(individuo i);
bool tieneEspaciosReservadosParaElTrabajo(hogar h);
bool suHogarEsCasaODepartamento(individuo i, eph_h th);
bool suHogarTieneEspaciosReservadosParaElTrabajo(individuo i, eph_h &th);
bool individuoEnHogarValido(individuo i, eph_h th);
bool trabajaEnSuVivienda(individuo i, eph_h th);
int cantIndividuosQueTrabajan(eph_h th, eph_i ti);
int cantIndividuosTrabajandoEnSuvivienda(eph_h th, eph_i ti);
float proporcionTeleworking(eph_h &th, eph_i &ti);

/*Proc 5*/
vector<vector<int>> auxOrdenarPorRegion(eph_h th);
vector<vector<int>> auxOrdenarPorCodusuEnRegion(eph_h th);
vector<vector<int>> auxOrdenarPorCodusuIndividuos(eph_h th, eph_i ti);
int finDeBloque(int n, eph_i ti);
vector<vector<int>> auxOrdenarPorComponenteAIndividuos(eph_h &th, eph_i ti);

/*Proc 6*/
int ingresos(hogar &h, eph_i &ti);
int difIngresos(eph_i &ti, hogar h1, hogar h2);
int indiceDeHogarConIngresoMinEnSubsec(eph_h &th, eph_i ti, int l, int r);
void ordenarTablaHogaresPorIngresos(eph_h &th, eph_i &ti);
vector<hogar> generarMuestraHomogeneaConDifEntreHogIyHogJ(eph_h th, eph_i ti, int i, int j);
vector<vector<hogar> > generarMuestrasHomogeneas(eph_h &th, eph_i &ti);
bool existeSolucionMuestraHomogeneaConAlMenos3(eph_h th, eph_i ti);
vector<hogar> mayorMuestraHomogenea(eph_h th, eph_i ti);

/*Proc 7*/
void modificaRegionGBAaPampeana(eph_h &th, eph_i &ti);

/*Proc 8*/
int indiceDeHogConIngresoMinPerCapitaEnSubsec(eph_h &th, eph_i &ti, int l, int r);
void ordenarTablaHogaresPorIngresosPerCapita(eph_h th, eph_i ti);
float areaObservada(eph_h &th, eph_i &ti);
float integralDeIngresosObservados(eph_h &th,eph_i &ti);
float ingresosHasta(int n, eph_h th, eph_i ti);
float ingresosPerCapitaEnHogar(hogar h, eph_i ti);
float calcularIndiceDeGini(eph_h th, eph_i ti);

#endif //SOLUCION_AUXILIARES_H
