#include "../definiciones.h"
#include "../Funciones_TPI.h"
#include "../ejercicios.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include "../auxiliares.h"

using namespace std;


TEST(ordenarRegionYCODUSUTEST, unSoloHogarYUnSoloIndividuo) {
    eph_h th_esperado = {{334, 2017, 1, 1, 2, 0, 1, 3, 1, 2}};
    eph_i ti_esperado = {{334, 1, 2017, 1, 0, 1, 36, 1, 3, 16300, 1}};

    eph_h th = {{334, 2017, 1, 1, 2, 0, 1, 3, 1, 2}};
    eph_i ti = {{334, 1, 2017, 1, 0, 1, 36, 1, 3, 16300, 1}};

    ordenarRegionYCodusu(th, ti);

    EXPECT_EQ(th_esperado, th);
    EXPECT_EQ(ti_esperado, ti);

}

TEST(ordenarRegionYCODUSUTEST, regionesDistintas) {

    eph_h th = {
            {334, 2017, 1, 1, 2, 0, 1, 3, 1, 2},
            {960, 2017, 1, 1, 1, 0, 1, 4, 1, 2},
    };

    eph_i ti = {
            {334, 1, 2017, 1, 0, 1, 36, 1, 3, 16300, 1},
            {960, 3, 2017, 1, 1, 2, 51, 1, 4, 1280,  1},
            {960, 2, 2017, 1, 0, 2, 20, 1, 3, 7000,  1},
            {960, 1, 2017, 1, 0, 2, 19, 0, 0, 0,     6}
    };

    eph_h th_esperado = {
            {960, 2017, 1, 1, 1, 0, 1, 4, 1, 2},
            {334, 2017, 1, 1, 2, 0, 1, 3, 1, 2},
    };

    eph_i ti_esperado = {
            {960, 1, 2017, 1, 0, 2, 19, 0, 0, 0,     6},
            {960, 2, 2017, 1, 0, 2, 20, 1, 3, 7000,  1},
            {960, 3, 2017, 1, 1, 2, 51, 1, 4, 1280,  1},
            {334, 1, 2017, 1, 0, 1, 36, 1, 3, 16300, 1},
    };


    ordenarRegionYCodusu(th, ti);
    EXPECT_EQ(th_esperado, th);
    EXPECT_EQ(ti_esperado, ti);

}

TEST(elSuperTEST, tablaGrande) {

    eph_h th = {
            {{350, 2008, 1, 1, 1, 0, 1, 5, 2, 1},
                    {542, 2004, 2, 2, 1, 0, 1, 5, 2, 2},
                    {754, 2004, 2, 2, 1, 0, 2, 6, 3, 2},
                    {1788, 2014, 1, 2, 4, 0, 5, 3, 1, 1},
                    {1601, 2014, 2, 3, 5, 0, 5, 5, 4, 2},
                    {2005, 2001, 2, 1, 6, 0, 1, 4, 2, 2},
                    {3001, 2001, 3, 2, 2, 1, 1, 4, 2, 1},
                    {895, 2007, 3, 2, 2, 1, 2, 2, 1, 2},
                    {970, 2010, 1, 3, 2, 0, 2, 6, 4, 1},
                    {1053, 2009, 2, 1, 3, 0, 3, 3, 2, 2},
                    {1534, 1998, 2, 2, 3, 0, 3, 4, 2, 2},
                    {2015, 1989, 3, 2, 4, 1, 4, 4, 3, 2},
            },
    };

    eph_i ti = {
            {2015, 1, 1989, 3, 2, 25, 1, 1,  3, 20000, 8},
            {1788, 1, 2014, 1, 1, 37, 0, -1, 0, 0,     6},
            {2005, 1, 2010, 1, 1, 18, 0, 1,  0, 0,     1},
            {3001, 1, 2010, 1, 2, 25, 1, 1,  3, 25000, 8},
            {895,  1, 2007, 3, 2, 45, 1, 1,  3, 30000, 6},
            {970,  1, 2010, 1, 1, 7,  0, -1, 0, 0,     6},
            {970,  2, 2010, 1, 2, 20, 0, 0,  0, 0,     6},
            {1053, 1, 2009, 2, 1, 37, 0, -1, 0, 0,     10},
            {350,  2, 2008, 1, 2, 20, 0, 0,  0, 0,     10},
            {350,  1, 2008, 1, 1, 7,  0, -1, 0, 0,     6},
            {542,  1, 2004, 2, 1, 18, 0, 1,  0, 0,     10},

    };

    eph_h th_esperado = {
            {350,  2008, 1, 1, 1, 0, 1, 5, 2, 1},
            {542,  2004, 2, 2, 1, 0, 1, 5, 2, 2},
            {754,  2004, 2, 2, 1, 0, 2, 6, 3, 2},
            {895,  2007, 3, 2, 2, 1, 2, 2, 1, 2},
            {970,  2010, 1, 3, 2, 0, 2, 6, 4, 1},
            {3001, 2001, 3, 2, 2, 1, 1, 4, 2, 1},
            {1053, 2009, 2, 1, 3, 0, 3, 3, 2, 2},
            {1534, 1998, 2, 2, 3, 0, 3, 4, 2, 2},
            {1788, 2014, 1, 2, 4, 0, 5, 3, 1, 1},
            {2015, 1989, 3, 2, 4, 1, 4, 4, 3, 2},
            {1601, 2014, 2, 3, 5, 0, 5, 5, 4, 2},
            {2005, 2001, 2, 1, 6, 0, 1, 4, 2, 2},
    };

    eph_i ti_esperado = {
            {350,  1, 2008, 1, 1, 7,  0, -1, 0, 0,     6},
            {350,  2, 2008, 1, 2, 20, 0, 0,  0, 0,     10},
            {542,  1, 2004, 2, 1, 18, 0, 1,  0, 0,     10},
            {895,  1, 2007, 3, 2, 45, 1, 1,  3, 30000, 6},
            {970,  1, 2010, 1, 1, 7,  0, -1, 0, 0,     6},
            {970,  2, 2010, 1, 2, 20, 0, 0,  0, 0,     6},
            {3001, 1, 2010, 1, 2, 25, 1, 1,  3, 25000, 8},
            {1053, 1, 2009, 2, 1, 37, 0, -1, 0, 0,     10},
            {1788, 1, 2014, 1, 1, 37, 0, -1, 0, 0,     6},
            {2015, 1, 1989, 3, 2, 25, 1, 1,  3, 20000, 8},
            {2005, 1, 2010, 1, 1, 18, 0, 1,  0, 0,     1},
    };


    ordenarRegionYCodusu(th, ti);
    EXPECT_EQ(th_esperado, th);
    EXPECT_EQ(ti_esperado, ti);

}

TEST(elSuperTEST2, tablaGrande) {

    eph_h th = {
            {589, 2007, 2, 1, 3, 0, 1, 5, 2, 1},
            {522, 2007, 2, 1, 2, 0, 1, 5, 2, 1},
            {700, 2007, 2, 1, 2, 0, 1, 5, 2, 1},
    };

    eph_i ti = {
            {700, 3, 2007, 2, 1, 9,  0, 0,  0, 0,     6},
            {589, 1, 2007, 2, 1, 62, 0, 1,  4, 0,     6},
            {589, 3, 2007, 2, 2, 25, 1, 1,  3, 20000, 8},
            {522, 3, 2007, 2, 1, 18, 0, 1,  0, 0,     1},
            {522, 4, 2007, 2, 2, 68, 1, 1,  2, -1,    7},
            {589, 2, 2007, 2, 1, 9,  0, 0,  0, 0,     6},
            {522, 1, 2007, 2, 2, 25, 1, 1,  3, 25000, 8},
            {522, 2, 2007, 2, 2, 55, 1, 1,  1, 40000, 1},
            {700, 1, 2007, 2, 1, 7,  0, -1, 0, 0,     6},
            {700, 2, 2007, 2, 1, 62, 0, 1,  4, 0,     6},

    };

    eph_h th_esperado = {
            {522, 2007, 2, 1, 2, 0, 1, 5, 2, 1},
            {700, 2007, 2, 1, 2, 0, 1, 5, 2, 1},
            {589, 2007, 2, 1, 3, 0, 1, 5, 2, 1},
    };

    eph_i ti_esperado = {
            {522, 1, 2007, 2, 2, 25, 1, 1,  3, 25000, 8},
            {522, 2, 2007, 2, 2, 55, 1, 1,  1, 40000, 1},
            {522, 3, 2007, 2, 1, 18, 0, 1,  0, 0,     1},
            {522, 4, 2007, 2, 2, 68, 1, 1,  2, -1,    7},
            {700, 1, 2007, 2, 1, 7,  0, -1, 0, 0,     6},
            {700, 2, 2007, 2, 1, 62, 0, 1,  4, 0,     6},
            {700, 3, 2007, 2, 1, 9,  0, 0,  0, 0,     6},
            {589, 1, 2007, 2, 1, 62, 0, 1,  4, 0,     6},
            {589, 2, 2007, 2, 1, 9,  0, 0,  0, 0,     6},
            {589, 3, 2007, 2, 2, 25, 1, 1,  3, 20000, 8},
    };


    ordenarRegionYCodusu(th, ti);
    EXPECT_EQ(th_esperado, th);
    EXPECT_EQ(ti_esperado, ti);

}