#include "definiciones.h"
#include "ejercicios.h"
#include "auxiliares.h"


bool esEncuestaValida(eph_h th, eph_i ti) {
    return esValida(th, ti);
}

vector<int> histHabitacional(eph_h th, eph_i ti, int region) {
    vector<int> res = {};
    int j = 1;
    int max = cantMaxHabitacionesEnHogEnReg(th, region);
    while (j <= max) {
        res.push_back(cantHogaresCasaConNHabitaciones(th, region, j));
        j++;
    }
    return res;
}

vector<float> laCasaEstaQuedandoChica(eph_h th, eph_i ti) {
    vector<float> res(CANTIDAD_DE_REGIONES);
    for (int i = 0; i < res.size(); i++) {
        res[i] = proporcionDeCasasConHC(th, ti, i + 1);
    }
    return res;
}

bool creceElTeleworkingEnCiudadesGrandes(eph_h t1h, eph_i t1i, eph_h t2h, eph_i t2i) {
    return proporcionTeleworking(t2h, t2i) > proporcionTeleworking(t1h, t1i);
}

void ordenarRegionYCodusu(eph_h &th, eph_i &ti) {
    eph_h auxh = th;
    eph_i auxi = ti;
    th = auxOrdenarPorCodusuEnRegion(auxh);
    ti = auxOrdenarPorComponenteAIndividuos(th, auxi);
}

vector<hogar> muestraHomogenea(eph_h th, eph_i ti) {
    vector<hogar> res;
    ordenarTablaHogaresPorIngresos(th, ti);
    if (existeSolucionMuestraHomogeneaConAlMenos3(th, ti)) {
        res = mayorMuestraHomogenea(th, ti);
    }
    return res;
}

void corregirRegion(eph_h &th, eph_i ti) {
    modificaRegionGBAaPampeana(th,ti);
}

float indiceGini(eph_h th, eph_i ti) {
    ordenarTablaHogaresPorIngresosPerCapita(th,ti);
    return calcularIndiceDeGini(th,ti);
}